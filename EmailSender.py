import smtplib
import EmailSenderConfig as config


class EmailSender:
    def __init__(self, subject, content, recipient):
        self.recipient = recipient
        self.subject = subject
        self.content = content

    def send(self):
        server = self.setup_server()
        server.sendmail(config.email, self.recipient, self.get_subject_and_content())
        server.quit()

    def setup_server(self):
        server = smtplib.SMTP_SSL(config.host, config.port)
        server.login(config.email, config.password)
        return server

    def get_subject_and_content(self):
        return 'Subject: {}\n\n{}'.format(self.subject, self.content)

    
    

    

