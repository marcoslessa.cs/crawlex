import Article
from WebsiteDao import insert_web_site, get_website_id_by_domain
from ExampleArticlesDao import insert_example_article
from urllib.parse import urlparse


class WebSiteConfig:
    def __init__(self, id: int, name: str, example_articles: [],
                 text_similarity_coefficient: float, minutes_to_crawl: int):
        self.__id = id
        self.__name = name
        self.__example_articles = example_articles
        self.__text_similarity_coefficient = text_similarity_coefficient
        self.__minutes_to_crawl = minutes_to_crawl
        self.__start_url = None
        self.__articles_url_pattern = None
        self.__title_extractor = None
        self.__subtitle_extractor = None
        self.__content_extractor = None
        self.__author_extractor = None
        self.__date_extractor = None

    def get_id(self):
        return self.__id

    def get_name(self):
        return self.__name

    def get_star_url(self):
        return self.__start_url

    def get_example_articles(self):
        return self.__example_articles

    def get_text_similarity_coefficient(self):
        return self.__text_similarity_coefficient

    def get_minutes_to_crawl(self):
        return self.__minutes_to_crawl

    def get_articles_url_pattern(self):
        return self.__articles_url_pattern

    def get_title_extractor(self):
        return self.__title_extractor

    def get_subtitle_extractor(self):
        return self.__subtitle_extractor

    def get_content(self):
        return self.__content_extractor

    def get_author(self):
        return self.__author_extractor

    def get_date(self):
        return self.__date_extractor


class WebsiteConfigGenerator:
    def __init__(self, example_articles: []):
        self.__example_articles = example_articles

    def generate(self) -> str:
        examples_copy = self.__example_articles.copy()
        random_article = examples_copy.pop()
        website_protocol = urlparse(random_article.get_url()).scheme
        website_domain = urlparse(random_article.get_url()).netloc
        website_start_url = website_protocol + '://' + website_domain
        insert_web_site(website_start_url)
        website_id = get_website_id_by_domain(website_start_url)
        for example_article in self.__example_articles:
            insert_example_article(website_id, example_article)
        return website_start_url







