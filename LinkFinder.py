import re
import sys

import requests
import urllib.parse
from bs4 import BeautifulSoup


class LinkFinder:
    def __init__(self, url_to_extract: str):
        self.__url_to_extract = url_to_extract
        self.__VALID_LINK_PATTERN = 'https?://.*'

    def find_links(self) -> set:
        possible_links = set()
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'}
        response = requests.get(self.__url_to_extract, headers=headers)
        html_from_response = response.content
        soup = BeautifulSoup(html_from_response, 'html5lib')
        for tag in soup.find_all(href=True):
            possible_link = tag.get('href')
            if str(possible_link).startswith('/'):
                possible_links.add(self.__url_to_extract + str(possible_link))
            else:
                possible_links.add(str(possible_link))
        links = self.__remove_no_links(possible_links)
        links = self.__remove_links_from_another_domain(possible_links)
        return links

    def __remove_no_links(self, possible_links: set) -> set:
        interactive_possible_links = possible_links.copy()
        for possible_link in interactive_possible_links:
            if not bool(re.fullmatch(self.__VALID_LINK_PATTERN, possible_link)):
                possible_links.remove(possible_link)
        return possible_links

    def __remove_links_from_another_domain(self, possible_links: set) -> set:
        interactive_possible_links = possible_links.copy()
        website_domain = urllib.parse.urlparse(self.__url_to_extract).netloc
        for possible_link in interactive_possible_links:
            if not urllib.parse.urlparse(possible_link).netloc == website_domain:
                possible_links.remove(possible_link)
        return possible_links











