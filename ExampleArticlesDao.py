import sqlite3
from Article import Article


def insert_example_article(website_id: int, article: Article):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("INSERT INTO ExampleArticle VALUES (?, ?, ?, ?, ?, ?, ?)", (website_id, article.get_url(),
                                                                                   article.get_title(), article.content,
                                                                                   article.get_date(),
                                                                                   article.get_subtitle(),
                                                                                   article.get_author()))
    conn.close()


def get_all_example_articles():
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("SELECT * FROM ExampleArticle")
        return cursor.fetchall()


def get_example_articles_by_website_id(website_id: int):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("SELECT * FROM ExampleArticle WHERE website_id = ?", (website_id,))
        results = cursor.fetchall()
        articles = []
        for article in results:
            url = article[1]
            title = article[2]
            content = article[3]
            date = article[4]
            subtitle = article[5]
            author = article[6]
            articles.append(Article(url, title, content, date, subtitle, author))
    conn.close()
    return articles

def remove_articles(website_id: int):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE FROM ExampleArticle WHERE website_id = ? and url == 'https://www.folhape.com.br/noticias/as-proximas-semanas-nao-serao-faceis-diz-margareth-dalcolmo/229983/'", (website_id,))
    conn.close()







