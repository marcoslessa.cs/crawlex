import sys

import bs4
import ExtractionConfig
import requests
from bs4 import BeautifulSoup
import Article
import re
import DefaultDatePatternConverter


class Extractor:

    def __init__(self, extraction_config: ExtractionConfig):
        self.__extraction_config = extraction_config

    def extract(self, url_to_extract):
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'}
        response = requests.get(url_to_extract, headers=headers)
        html = response.content
        soup = BeautifulSoup(html, 'html5lib')
        try:
            title = self.extract_title(soup)
            subtitle = self.extract_subtitle(soup)
            content = self.extract_content(soup)
            author = self.extract_author(soup)
            date = self.extract_date(soup)
        except Exception as e:
            print(e)
        return Article.Article(url_to_extract, title, content, author=author, subtitle=subtitle, date=date)

    def extract_title(self, soup: BeautifulSoup):
        extract_element = soup.find(self.__extraction_config.get_title_extractor().name,
                                    self.__extraction_config.get_title_extractor().attrs)
        if extract_element is not None:
            return extract_element.text
        raise Exception("Unable to extract title")

    def extract_subtitle(self, soup: BeautifulSoup):
        extract_element = None
        if self.__extraction_config.get_subtitle_extractor() is not None:
            extract_element = soup.find(self.__extraction_config.get_subtitle_extractor().name,
                                        self.__extraction_config.get_subtitle_extractor().attrs)
        if extract_element is not None:
            return extract_element.text
        return None

    def extract_content(self, soup: BeautifulSoup):
        possible_elements = soup.find_all(self.__extraction_config.get_content_extractor().name,
                                          self.__extraction_config.get_content_extractor().attrs)
        extract_element = None
        for possible_element in possible_elements:
            if possible_element.attrs == self.__extraction_config.get_content_extractor().attrs and \
                    possible_element.name == self.__extraction_config.get_content_extractor().name:
                extract_element = possible_element
                break
        cleaner_elements = self.__extraction_config.get_content_cleaner_element()


        #cleaner
        for tag in extract_element.find_all():
            for cleaner_element in cleaner_elements:
                if tag.name == 'div' and str(tag.attrs) == '{}':
                    continue
                if tag.name == cleaner_element[0] and str(tag.attrs) == cleaner_element[1]:
                    print('printando o nome ', tag.name, tag.attrs)
                    tag.decompose()

        #end_cleaner

        for tag in extract_element.find_all(['style', 'script']):
            tag.decompose()

        content = ''
        if extract_element is not None:
            for element in extract_element:
                if isinstance(element, bs4.Tag):
                    block = element.text.strip()
                    if block == '':
                        continue
                    content += block + '\n\n'
            return content
        raise Exception("Unable to extract content")

    def extract_author(self, soup: BeautifulSoup):
        extract_element = None
        if self.__extraction_config.get_author_extractor() is not None:
            extract_element = soup.find(self.__extraction_config.get_author_extractor().name,
                                        self.__extraction_config.get_author_extractor().attrs)
        if extract_element is not None:
            return extract_element.text
        return None

    def extract_date(self, soup: BeautifulSoup):
        if self.__extraction_config.get_date_extractor() is None:
            return None
        tag_name = self.__extraction_config.get_date_extractor()[0].name
        tag_attrs = self.__extraction_config.get_date_extractor()[0].attrs
        extract_element = soup.find(tag_name, tag_attrs)
        if extract_element is not None:
            date_regex = self.__extraction_config.get_date_extractor()[2]
            date_format = self.__extraction_config.get_date_extractor()[1]
            date_result = re.search(date_regex, extract_element.text)
            if date_result is not None:
                date = date_result.group()
                default_date_pattern_converter = DefaultDatePatternConverter.DefaultDatePatternConverter((date,
                                                                                                          date_format))
                return default_date_pattern_converter.convert_date()
        return None



