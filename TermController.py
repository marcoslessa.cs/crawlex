import Article


class TermController:
    def __init__(self, all_this_words: set(), exactly_this_phrase: str, any_of_this_words: set()):
        self.__all_this_words = all_this_words
        self.__exactly_this_phrase = exactly_this_phrase
        self.__any_of_this_words = any_of_this_words

    def have_all_this_words(self, article: Article):
        for word in self.__any_of_this_words:
            if article.get_title().lower() is not None and word.lower() not in article.get_title().lower():
                return False
            if article.get_subtitle().lower() is not None and word.lower() not in article.get_subtitle().lower():
                return False
            if article.get_content().lower() is not None and word.lower() not in article.get_content().lower():
                return False
            if article.get_author().lower() is not None and word.lower() not in article.get_author().lower():
                return False
            if article.get_date().lower() is not None and word.lower() not in article.get_date().lower():
                return False
        return True

    def has_exactly_this_phrase(self, article: Article):
        if article.get_title().lower() is not None and self.__exactly_this_phrase.lower() in article.get_title().lower():
            return True
        if article.get_subtitle().lower() is not None and self.__exactly_this_phrase.lower() in article.get_subtitle().lower():
            return True
        if article.get_content().lower() is not None and self.__exactly_this_phrase.lower() in article.get_content().lower():
            return True
        if article.get_author().lower() is not None and self.__exactly_this_phrase.lower() in article.get_author().lower():
            return True
        if article.get_date().lower() is not None and self.__exactly_this_phrase.lower() in article.get_date().lower():
            return True

    def have_any_of_this_words(self, article: Article):
        for word in self.__any_of_this_words:
            if article.get_title().lower() is not None and word.lower() in article.get_title().lower():
                return True
            if article.get_subtitle().lower() is not None and word.lower() in article.get_subtitle().lower():
                return True
            if article.get_content().lower() is not None and word.lower() in article.get_content().lower():
                return True
            if article.get_author().lower() is not None and word.lower() in article.get_author().lower():
                return True
            if article.get_date().lower() is not None and word.lower() in article.get_date().lower():
                return True
        return False

    def is_valid(self, article: Article):
        return self.have_any_of_this_words(article) or self.has_exactly_this_phrase(article) or \
               self.have_all_this_words(article)

    def __str__(self):
        to_string = "Filtros de interesse: \n"
        to_string += "Todas estas palavras: " + str(self.__all_this_words) + "\n"
        to_string += "Quaisquer destas palavras: " + str(self.__any_of_this_words) + "\n"
        to_string += "Exatamente esta frase: " + self.__exactly_this_phrase + "\n\n"
        return to_string
