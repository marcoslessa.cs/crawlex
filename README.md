## Requirements
- python 3.8.10
- html5lib 1.1
- PyQt5 5.15.6
- requests 2.22.0
- beautifulsoup4 4.8.2

## To run, in crawlex/
**python3 crawlex.py**
