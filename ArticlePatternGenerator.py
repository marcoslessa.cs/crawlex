from urllib.parse import urlparse as urlParse
import re


class ArticlePatternGenerator:
    def __init__(self, articleUrls=[]):
        self.articleUrls = articleUrls

    def getArticleUrls(self):
        return self.articleUrls

    def setUrl(self, articleUrls):
        self.articleUrls = articleUrls

    def generate(self):
        parseResult = urlParse(self.articleUrls[0])
        regex = parseResult.scheme + "://" + parseResult.netloc + "/"
        tuplePathsList = []
        result_regex = ''
        for url in self.articleUrls:
            urlPath = urlParse(url).path
            url_full_path = urlPath
            if urlParse(url).query is not None:
                url_full_path += '?' + urlParse(url).query
            urlPathList = url_full_path.split("/")
            if urlPathList[-1] == "":
                urlPathList.pop()
            if urlPathList[0] == "":
                urlPathList.pop(0)
            tuplePathsList.append(tuple(urlPathList))
        if self.__isPathsSameSize(tuplePathsList):
            result_regex = regex + self.__generateForSamePathsSize(tuplePathsList)
        else:
            result_regex = regex + self.__generateForDifferentPathsSize(tuplePathsList)
        if result_regex.endswith('?'):
            return result_regex
        else:
            return result_regex + '?'

    def __isPathsSameSize(self, tuplePathsList):
        firstTuplePathsSize = len(tuplePathsList[0])
        for tuplePaths in tuplePathsList:
            if len(tuplePaths) != firstTuplePathsSize:
                return False
        return True

    def __generateForSamePathsSize(self, tuplePathsList):
        regexForPath = ""
        tuplesSize = self.__getMinimumSizePath(tuplePathsList)
        index = 0
        while index < tuplesSize:
            indexContent = []
            for tuple in tuplePathsList:
                indexContent.append(tuple[index])
            regexForPath += self.__generateForIndexContent(indexContent) + "/"
            index += 1
        return regexForPath + "?"

    def __generateForDifferentPathsSize(self, tuplePathsList):
        regexForPath = self.__generateForSamePathsSize(tuplePathsList)
        optionalPath = regexForPath + "("
        minimumSizePath = self.__getMinimumSizePath(tuplePathsList)
        maximumSizePath = self.__getMaximumSizePath(tuplePathsList)
        for _ in range(maximumSizePath - minimumSizePath):
            optionalPath += '[^/]+/'
        return optionalPath + "?)"

    def __generateForIndexContent(self, indexContent):
        if self.__isEquals(indexContent) and self.__isNumber(indexContent) is False:
            return indexContent[0]
        elif self.__isNumber(indexContent):
            return '\d+'
        return '[^/]+'

    def __isEquals(self, indexContent):
        baseContent = indexContent[0]
        for content in indexContent:
            if content != baseContent:
                return False
        return True

    def __isNumber(self, indexContent):
        baseContent = indexContent[0]
        for content in indexContent:
            if bool(re.fullmatch('\d+', content)) == False:
                return False
        return True

    def __getMinimumSizePath(self, tuplePathsList):
        minimumSize = len(tuplePathsList[0])
        for tuple in tuplePathsList:
            if len(tuple) < minimumSize:
                minimumSize = len(tuple)
        return minimumSize

    def __getMaximumSizePath(self, tuplePathsList):
        maximumSize = len(tuplePathsList[0])
        for tuple in tuplePathsList:
            if len(tuple) > maximumSize:
                maximumSize = len(tuple)
        return maximumSize



