import sqlite3


def insert_web_site(domain: str):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("INSERT INTO Website VALUES (?, ?)", (None, domain))
    conn.close()


def get_all_websites() -> tuple:
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("SELECT * FROM Website")
        result = cursor.fetchall()
    conn.close()
    return result


def get_all_websites_ids() -> []:
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("SELECT id FROM Website")
        results = cursor.fetchall()
        ids = []
        for result in results:
            ids.append(result[0])
        ids.sort()
        return ids


def get_website_id_by_domain(domain: str):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
       result = cursor.execute("SELECT id FROM Website WHERE domain = ?", (domain,)).fetchone()[0]
    conn.close()
    return result

def delete_website_by_domain(domain: str):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE FROM Website WHERE domain = ?", (domain,))
    conn.close()

def delete_all_websites():
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE FROM Website")
    conn.close()


def get_examples_articles(id: str):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE FROM Website")
        return cursor.fetchall

def delete_website_by_id(id: int):
    conn = sqlite3.connect("database.db")
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE FROM Website WHERE id = ?", (id,))
    conn.close()
