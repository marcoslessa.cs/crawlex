import LinkFinder
from WebsiteDao import get_website_id_by_domain
from ExampleArticlesDao import get_example_articles_by_website_id
import ArticlePatternGenerator
import re
import ExtractionConfig
import Extractor
import time
import TermController
from ArticleDao import insert_article


class WebsiteExecutor:
    def __init__(self, start_url: str, seconds_to_execute: int, term_controller: TermController):
        self.__start_url = start_url
        self.__start_time = None
        self.__seconds_to_execute = seconds_to_execute
        self.__term_controller = term_controller
        self.__urls_already_visited = set()
        self.__urls_to_visit = set()
        self.__urls_to_visit_after = set()
        self.__example_articles = self.load_examples_articles_from_website()
        self.__ARTICLE_PATTERN_TO_EXTRACT = self.generate_article_pattern_to_extract()
        self.__extraction_config = self.generate_extraction_config()
        self.__extractor = Extractor.Extractor(self.__extraction_config)

    def execute(self):
        self.__start_time = time.time()
        self.__urls_already_visited.add(self.__start_url)
        link_finder = LinkFinder.LinkFinder(self.__start_url)
        self.__urls_to_visit = link_finder.find_links()

        while len(self.__urls_to_visit) > 0:
            if self.stop_crawl():
                print('Tempo de execução esgotado para ' + self.__start_url + "\n")
                return

            # seleciona a url cabeca da lista, e remove
            new_url = self.__urls_to_visit.pop()

            # verifica se a url já foi visitada
            if new_url in self.__urls_already_visited:
                continue

            # se nao foi visitada, marca como visitada
            self.__urls_already_visited.add(new_url)
            link_finder = LinkFinder.LinkFinder(new_url)

            try:
                self.__urls_to_visit_after = self.__urls_to_visit_after.union(link_finder.find_links())
            except:
                print('Erro ao buscar urls na na página: ', new_url)
                print('')
                continue

            # tenta extrair o conteudo da new_url
            # TODO: extract(new_url)
            # self.update_log.emit(new_url)
            print('Nova URL: ', new_url)
            print('')
            print('')

            # tenta extrair caso passe na expressao de documento
            if self.match_article_pattern(new_url):
                try:
                    print("Tentando extrair possível artigo em: " + new_url)
                    extracted_article = self.__extractor.extract(new_url)
                    if self.__term_controller.is_valid(extracted_article):
                        insert_article(extracted_article)
                except Exception as e:
                    print(e)

        if len(self.__urls_to_visit_after) == 0:
            return
        self.__start_url = self.__urls_to_visit_after.pop()
        return self.run()

    def match_article_pattern(self, url):
        return bool(re.fullmatch(self.__ARTICLE_PATTERN_TO_EXTRACT, url))

    def stop_crawl(self):
        time_now = time.time()
        return (time_now - self.__start_time) >= self.__seconds_to_execute

    def load_examples_articles_from_website(self):
        website_id = get_website_id_by_domain(self.__start_url)
        return get_example_articles_by_website_id(website_id)

    def generate_article_pattern_to_extract(self):
        example_articles_url = []
        for example_article in self.__example_articles:
            example_articles_url.append(example_article.get_url())
        article_pattern_generator = ArticlePatternGenerator.ArticlePatternGenerator(example_articles_url)
        return article_pattern_generator.generate()

    def generate_extraction_config(self):
        extraction_config = ExtractionConfig.ExtractionConfig(self.__example_articles)
        print('Gerando configuração para coleta do web site ' + self.__start_url)
        extraction_config.generate_config()
        return extraction_config
