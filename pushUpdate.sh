#!/bin/bash

if [ -z $1 ]
then
    echo "you must pass a commit message"
    exit
fi

git add --all
git status
git commit -m "$*"
git status
git push