import threading
import WebsiteExecutor
from PyQt5.QtWidgets import QMessageBox
import TermController
from ArticleDao import delete_all_articles, get_all_articles
import EmailSender


class SharedCrawler(threading.Thread):
    def __init__(self, name, start_urls, execution_seconds_per_website: int, term_controller: TermController,
                 emails_to_be_sent: set()):
        threading.Thread.__init__(self)
        self.__name = name
        self.__start_urls = start_urls
        self.__execution_seconds_per_website = execution_seconds_per_website
        self.__term_controller = term_controller
        self.__email_to_be_sent = emails_to_be_sent

    def run(self):
        for start_url in self.__start_urls:
            print("Iniciando coleta para " + start_url + "\n")
            website_executor = WebsiteExecutor.WebsiteExecutor(start_url, self.__execution_seconds_per_website,
                                                               self.__term_controller)
            try:
                website_executor.execute()
            except Exception as e:
                print(e)
        try:
            self.send_email()
        except Exception as e:
            print(e)
        delete_all_articles()
        QMessageBox.information(None, "INFO", "Execução do crawler " + self.__name + " finalizada!")

    def send_email(self):
        message = "Relatorio de coleta para o Crawler " + self.__name + ": \n\n"
        message += str(self.__term_controller)
        message += self.get_articles_url()
        message = message.encode("ascii", errors="replace")
        print(message.decode())
        emailSender = EmailSender.EmailSender("Relatorio coleta: Crawler " + self.__name, message.decode(),
                                  list(self.__email_to_be_sent))
        emailSender.send()

    def get_articles_url(self):
        urls = "URLs: \n"
        for article in get_all_articles():
            urls += article[0] + "\n"
        return urls

