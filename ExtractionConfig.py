import sys

import bs4
import re
import ContentComparator
import requests
from bs4 import BeautifulSoup
from DatePatternGenerator import DatePatternRegexExtractorGenerator


class ExtractionConfig:
    def __init__(self, example_articles: []):
        self.__example_articles = example_articles
        self.__title_extractor_element = None
        self.__subtitle_extractor_element = None
        self.__content_extractor_element = None
        self.__author_extractor_element = None
        self.__date_extractor_element = None
        self.__content_cleaner_element = None
        self.__full_html_content = self.load_full_html_content_dict()

    def get_title_extractor(self):
        return self.__title_extractor_element

    def get_subtitle_extractor(self):
        return self.__subtitle_extractor_element

    def get_content_extractor(self):
        return self.__content_extractor_element

    def get_content_cleaner_element(self):
        return self.__content_cleaner_element

    def get_author_extractor(self):
        return self.__author_extractor_element

    def get_date_extractor(self):
        return self.__date_extractor_element

    def generate_config(self):
        self.search_title_extractor_element()
        self.search_subtitle_extractor_element()
        self.search_content_extractor_element()
        self.search_author_extractor_element()
        self.search_date_extractor_element()

    def search_title_extractor_element(self):
        possible_extractor_elements = set()
        first_example_article = self.__example_articles[0]
        soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
        for tag in soup.find_all():
            if tag.text.lower().__contains__(first_example_article.get_title().lower()):
                possible_extractor_elements.add((tag.name, str(tag.attrs)))

        possible_extractor_elements_copy = possible_extractor_elements.copy()

        for possible_extractor_element in possible_extractor_elements_copy:
            for example_article in self.__example_articles:
                soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
                tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
                if tag is not None:
                    if possible_extractor_elements.__contains__((tag.name, str(tag.attrs))):
                        if tag.text.lower().__contains__(example_article.get_title().lower()) is False:
                            possible_extractor_elements.remove((tag.name, str(tag.attrs)))
                            break
                else:
                    if possible_extractor_elements.__contains__((possible_extractor_element[0], possible_extractor_element[1])):
                        possible_extractor_elements.remove((possible_extractor_element[0], possible_extractor_element[1]))

        shortest_tag = None
        for possible_extractor_element in possible_extractor_elements:
            soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
            tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
            if tag is not None:
                if shortest_tag is None or len(shortest_tag.text) > len(tag.text):
                    if tag.text.lower().__contains__(first_example_article.get_title().lower()):
                        shortest_tag = tag
        print('shortest tag:', shortest_tag)
        self.__title_extractor_element = shortest_tag

    def search_subtitle_extractor_element(self):
        possible_extractor_elements = set()
        first_example_article = self.__example_articles[0]
        if first_example_article.get_subtitle() is None or first_example_article.get_subtitle() == '':
            self.__subtitle_extractor_element = None
            return
        soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
        for tag in soup.find_all():
            if tag.text.__contains__(first_example_article.get_subtitle()):
                possible_extractor_elements.add((tag.name, str(tag.attrs)))
                print(tag.name, str(tag.attrs))

        possible_extractor_elements_copy = possible_extractor_elements.copy()

        for possible_extractor_element in possible_extractor_elements_copy:
            for example_article in self.__example_articles:
                soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
                tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
                if tag is not None:
                    if possible_extractor_elements.__contains__((tag.name, str(tag.attrs))):
                        if tag.text.__contains__(example_article.get_subtitle()) is False:
                            possible_extractor_elements.remove((tag.name, str(tag.attrs)))
                            break

        shortest_tag = None
        for possible_extractor_element in possible_extractor_elements:
            soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
            tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
            if tag is not None:
                if shortest_tag is None or len(shortest_tag.text) > len(tag.text):
                    if tag.text.__contains__(first_example_article.get_subtitle()):
                        shortest_tag = tag
        print('shortest tag:', shortest_tag)
        self.__subtitle_extractor_element = shortest_tag

    def search_content_extractor_element(self):
        ratio = 0
        extractor = None
        for example_article in self.__example_articles:
            soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
            for tag in soup.find_all():
                content_comparator = ContentComparator.ContentComparator(example_article.get_content(), tag.text)
                if content_comparator.compare() > ratio:
                    ratio = content_comparator.compare()
                    extractor = tag
        self.__content_extractor_element = extractor

        base_article_content = None
        base_ratio = 0
        for example_article in self.__example_articles:
            content_comparator = ContentComparator.ContentComparator(example_article.get_content(), extractor.text)
            if content_comparator.compare() > base_ratio:
                base_ratio = content_comparator.compare()
                base_article_content = example_article.get_content()

        extractor_copy = extractor
        cleaner_elements = set()

        for tag in extractor_copy.find_all_next():
            if isinstance(tag, bs4.Tag):
                if not base_article_content.__contains__(tag.text):
                    str_tag = (tag.name, str(tag.attrs))
                    if str_tag[0] == 'p':
                        continue
                    tag.decompose()
                    content_comparator = ContentComparator.ContentComparator(base_article_content, extractor.text)
                    if content_comparator.compare() >= ratio:
                        ratio = content_comparator.compare()
                        if tag is not None:
                            cleaner_elements.add(str_tag)
        self.__content_cleaner_element = cleaner_elements

    def search_author_extractor_element(self):
        possible_extractor_elements = set()
        first_example_article = self.__example_articles[0]
        if first_example_article.get_author() is None or first_example_article.get_author() == '':
            self.__author_extractor_element = None
            return
        soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
        for tag in soup.find_all():
            if tag.text.__contains__(first_example_article.get_author()):
                possible_extractor_elements.add((tag.name, str(tag.attrs)))
                print(tag.name, str(tag.attrs))

        possible_extractor_elements_copy = possible_extractor_elements.copy()

        for possible_extractor_element in possible_extractor_elements_copy:
            for example_article in self.__example_articles:
                soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
                tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
                if tag is not None:
                    if possible_extractor_elements.__contains__((tag.name, str(tag.attrs))):
                        if tag.text.__contains__(example_article.get_author()) is False:
                            possible_extractor_elements.remove((tag.name, str(tag.attrs)))
                            break

        shortest_tag = None
        for possible_extractor_element in possible_extractor_elements:
            soup = BeautifulSoup(self.__full_html_content[first_example_article.get_url()], 'html5lib')
            tag = soup.find(possible_extractor_element[0], eval(possible_extractor_element[1]))
            if tag is not None:
                if shortest_tag is None or len(shortest_tag.text) > len(tag.text):
                    if tag.text.__contains__(first_example_article.get_author()):
                        shortest_tag = tag
        print('shortest tag:', shortest_tag)
        self.__author_extractor_element = shortest_tag

    def search_date_extractor_element(self):
        if self.__example_articles[0].get_date() is None or self.__example_articles[0].get_date() == '':
            self.__date_extractor_element = None
            return
        date_possible_extractor_elements = self.search_possible_date_extractor_elements()
        articles_with_date = self.load_articles_with_date()
        for date_possible_extractor_element in date_possible_extractor_elements.copy():
            for example_article in articles_with_date:
                date_pattern_regex_extractor_generator = DatePatternRegexExtractorGenerator(example_article.get_date())
                date_elements = date_pattern_regex_extractor_generator.generate_patterns_regex_extractor()
                soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
                tag = soup.find(date_possible_extractor_element[0].name, date_possible_extractor_element[0].attrs)
                if tag is not None:
                    text = tag.text
                ok = 0
                for date_element in date_elements:
                    date = date_element[0]
                    date_pattern = date_element[2]
                    try:
                        result = re.search(date_pattern, text)
                        if result is not None and result.group() == date:
                            ok = 1
                            break
                    except Exception as e:
                        print(e)
                        raise(e)
                if ok == 0:
                    date_possible_extractor_elements.remove(date_possible_extractor_element)
                    break

        short_date_possible_extractor_element = None
        for date_possible_extractor_element in date_possible_extractor_elements:
            if short_date_possible_extractor_element is None or len(date_possible_extractor_element[0]) < len(short_date_possible_extractor_element[0]):
                short_date_possible_extractor_element = date_possible_extractor_element

        self.__date_extractor_element = short_date_possible_extractor_element

    def load_articles_with_date(self):
        articles_with_date = set()
        for example_article in self.__example_articles:
            if example_article.get_date() is not None and example_article.get_date != '':
                articles_with_date.add(example_article)
        return articles_with_date

    def search_possible_date_extractor_elements(self):
        example_article = self.__example_articles[0]
        date_possible_extractor_elements = set()
        possible_extractor_elements = set()
        date_pattern_regex_extractor_generator = DatePatternRegexExtractorGenerator(example_article.get_date())
        date_elements = date_pattern_regex_extractor_generator.generate_patterns_regex_extractor()
        soup = BeautifulSoup(self.__full_html_content[example_article.get_url()], 'html5lib')
        for tag in soup.find_all():
            for element in date_elements:
                if tag.text.__contains__(element[0]):
                    possible_extractor_elements.add((tag, element))

        for possible_extractor_element in possible_extractor_elements:
            element = soup.find(possible_extractor_element[0].name, possible_extractor_element[0].attrs)
            if isinstance(element, bs4.Tag):
                text = element.text
                result = re.search(possible_extractor_element[1][2], text)
                if result is not None and result.group() == possible_extractor_element[1][0]:
                    date_possible_extractor_elements.add(
                        (element, possible_extractor_element[1][1], possible_extractor_element[1][2]))


        return date_possible_extractor_elements

    def load_full_html_content_dict(self):
        full_html_content_dict = {}
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'}
        for example_article in self.__example_articles:
            response = requests.get(example_article.get_url(), headers=headers)
            html = response.content
            full_html_content_dict[example_article.get_url()] = html
        return full_html_content_dict



