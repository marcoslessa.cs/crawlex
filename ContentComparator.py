import string
import difflib

"""Ratclif-Obershelp"""

class ContentComparator():
    def __init__(self, content_example: string, extracted_content: string):
        self.content_example = content_example
        self.extracted_content = extracted_content

    def compare(self) -> bool:
        sequence_matcher = difflib.SequenceMatcher(None, self.content_example, self.extracted_content)
        return sequence_matcher.ratio()