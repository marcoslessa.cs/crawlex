import sqlite3

conn = sqlite3.connect("database.db")
cursor = conn.cursor()

cursor.execute("""CREATE TABLE Article (
                url text,
                title text,
                content text,
                date text,
                subtitle text,
                author text
                )""")

cursor.execute("""CREATE TABLE Website (
                id INTEGER PRIMARY KEY,
                domain TEXT NOT NULL UNIQUE
                )""")

cursor.execute("""CREATE TABLE ExampleArticle (
                website_id INTEGER,
                url TEXT NOT NULL,
                title TEXT NOT NULL,
                content TEXT NOT NULL,
                date TEXT,
                subtitle TEXT,
                author TEXT
                )""")

conn.commit()
conn.close()