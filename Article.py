class Article:
    def __init__(self, url, title, content, date=None, subtitle=None, author=None):
        self.url = url
        self.title = title
        self.content = content
        self.date = date
        self.subtitle = subtitle
        self.author = author

    def get_url(self):
        return self.url
    
    def set_url(self, url):
        self.url = url

    def get_title(self):
        return self.title

    def set_title(self, title):
        self.title = title

    def get_content(self):
        return self.content

    def set_content(self, content):
        self.content = content

    def get_date(self):
        return self.date

    def set_date(self, date):
        self.date = date

    def get_subtitle(self):
        return self.subtitle

    def set_subtitle(self, subtitle):
        self.subtitle = subtitle

    def get_author(self):
        return self.author

    def set_author(self, author):
        self.author = author

    def __str__(self):
        fullArticle = "Título: " + self.title + "\n\n"
        if self.subtitle is not None and self.subtitle is not "":
            fullArticle += "Subtítulo: " + self.subtitle + "\n\n"
        fullArticle += "Conteúdo: " + self.content + "\n\n"
        if self.date is not None and self.date is not "":
            fullArticle += "Data de publicação: " + self.date + "\n\n"
        if self.author is not None and self.author is not "":
            fullArticle += "Autor: " + self.author + "\n\n"
        fullArticle += "Disponível no website: " + self.url
        return fullArticle


