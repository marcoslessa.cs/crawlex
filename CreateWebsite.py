import sqlite3

conn = sqlite3.connect("Website.db")
cursor = conn.cursor()
cursor.execute("""CREATE TABLE Website (
                id INTEGER PRIMARY KEY,
                name TEXT,
                domain TEXT NOT NULL UNIQUE
                )""")
conn.commit()
conn.close()