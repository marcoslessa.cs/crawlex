import sys
from ui import Ui_MainWindow
from PyQt5 import QtWidgets
from ArticleDao import delete_all_articles


if __name__ == "__main__":
    delete_all_articles()
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
        



