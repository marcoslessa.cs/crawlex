import locale
from datetime import datetime


class DatePatternRegexExtractorGenerator:
    def __init__(self, date: str):
        locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
        date_obj = datetime.strptime(date, '%a %b %d %Y')
        self.__default_date = str(date_obj.day) + '/' + str(date_obj.month) + '/' + str(date_obj.year)

    def generate_patterns_regex_extractor(self) -> set:
        locale.setlocale(locale.LC_TIME, 'pt_BR.UTF-8')
        date_time_obj = datetime.strptime(self.__default_date, '%d/%m/%Y')

        pt_regex = '\\d{1,2} de \\w+ de \\d{2}(\\d{2})?'
        default_barra_regex = '\\d{1,2}/\\d{1,2}/\\d{2}(\\d{2})?'
        default_traco_regex = '\\d{1,2}-\\d{1,2}-\\d{2}(\\d{2})?'
        year_first_barra_regex = '\\d{2}(\\d{2})?/\\d{1,2}/\\d{1,2}'
        year_first_traco_regex = '\\d{2}(\\d{2})?-\\d{1,2}-\\d{1,2}'

        date_patterns_regex_extractor = set()

        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%m/%Y'), '%d/%m/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%m/%y'), '%d/%m/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%-m/%Y'), '%d/%m/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%-m/%y'), '%d/%m/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%B/%Y'), '%d/%B/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%B/%y'), '%d/%B/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%b/%Y'), '%d/%b/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d/%b/%y'), '%d/%b/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%m/%Y'), '%d/%m/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%m/%y'), '%d/%m/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%-m/%Y'), '%d/%m/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%-m/%y'), '%d/%m/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%B/%Y'), '%d/%B/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%B/%y'), '%d/%B/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%b/%Y'), '%d/%b/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d/%b/%y'), '%d/%b/%y', default_barra_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%m-%Y'), '%d-%m-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%m-%y'), '%d-%m-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%-m-%Y'), '%d-%m-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%-m-%y'), '%d-%m-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%B-%Y'), '%d-%B-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%B-%y'), '%d-%B-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%b-%Y'), '%d-%b-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d-%b-%y'), '%d-%b-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%m-%Y'), '%d-%m-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%m-%y'), '%d-%m-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%-m-%Y'), '%d-%m-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%-m-%y'), '%d-%m-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%B-%Y'), '%d-%B-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%B-%y'), '%d-%B-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%b-%Y'), '%d-%b-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d-%b-%y'), '%d-%b-%y', default_traco_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%d de %B de %Y'), '%d de %B de %Y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d de %B de %y'), '%d de %B de %y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d de %b de %Y'), '%d de %b de %Y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%d de %b de %y'), '%d de %B de %y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d de %B de %Y'), '%d de %B de %Y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d de %B de %y'), '%d de %B de %y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d de %b de %Y'), '%d de %b de %Y', pt_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-d de %b de %y'), '%d de %B de %y', pt_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%m/%d/%Y'), '%m/%d/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m/%d/%y'), '%m/%d/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m/%-d/%Y'), '%m/%d/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m/%-d/%y'), '%m/%d/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m/%d/%Y'), '%m/%d/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m/%d/%y'), '%m/%d/%y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m/%-d/%Y'), '%m/%d/%Y', default_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m/%-d/%y'), '%m/%d/%y', default_barra_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%m-%d-%Y'), '%m-%d-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m-%d-%y'), '%m-%d-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m-%-d-%Y'), '%m-%d-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%m-%-d-%y'), '%m-%d-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m-%d-%Y'), '%m-%d-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m-%d-%y'), '%m-%d-%y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m-%-d-%Y'), '%m-%d-%Y', default_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%-m-%-d-%y'), '%m-%d-%y', default_traco_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y/%m/%d'), '%Y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y/%m/%d'), '%y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y/%m/%-d'), '%Y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y/%m/%-d'), '%y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y/%-m/%d'), '%Y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y/%-m/%d'), '%y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y/%-m/%-d'), '%Y/%m/%d', year_first_barra_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y/%-m/%-d'), '%y/%m/%d', year_first_barra_regex))

        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y-%m-%d'), '%Y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y-%m-%d'), '%y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y-%m-%-d'), '%Y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y-%m-%-d'), '%y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y-%-m-%d'), '%Y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y-%-m-%d'), '%y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%Y-%-m-%-d'), '%Y-%m-%d', year_first_traco_regex))
        date_patterns_regex_extractor.add((date_time_obj.strftime('%y-%-m-%-d'), '%y-%m-%d', year_first_traco_regex))

        return date_patterns_regex_extractor