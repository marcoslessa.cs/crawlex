import locale
from datetime import datetime


class DefaultDatePatternConverter:
    def __init__(self, date_and_pattern_extracted: tuple):
        self.date_and_pattern_extracted = date_and_pattern_extracted

    def convert_date(self):
        locale.setlocale(locale.LC_TIME, 'pt_BR.UTF-8')
        date_str = self.date_and_pattern_extracted[0]
        extracted_date_pattern = self.date_and_pattern_extracted[1]
        date_time_obj = datetime.strptime(date_str, extracted_date_pattern)
        date_in_default_pattern = date_time_obj.strftime('%d/%m/%Y')
        return date_in_default_pattern
