import sqlite3

conn = sqlite3.connect("ExampleArticles.db")
cursor = conn.cursor()
cursor.execute("""CREATE TABLE ExampleArticles (
                website_id INTEGER,
                url TEXT NOT NULL,
                title TEXT NOT NULL,
                content TEXT NOT NULL,
                date TEXT,
                subtitle TEXT,
                author TEXT,
                FOREIGN KEY (website_id)
                    REFERENCES Website (id) 
                        ON DELETE CASCADE 
                        ON UPDATE NO ACTION
                )""")
conn.commit()
conn.close()

