import sqlite3
from Article import Article

conn = sqlite3.connect("Articles.db", check_same_thread=False)
cursor = conn.cursor()


def insert_article(article: Article):
    with conn:
        cursor.execute("INSERT INTO Articles VALUES (?, ?, ?, ?, ?, ?)", (article.get_url(), article.get_title(),
                            article.get_content(), article.get_date(), article.get_subtitle(), article.get_author()))


def get_all_articles():
    with conn:
        cursor.execute("SELECT * FROM Articles")
        return cursor.fetchall()


def delete_all_articles():
    with conn:
        cursor.execute("DELETE FROM Articles")


def get_articles_by_boolean_query(boolean_query):
    with conn:
        cursor.execute("SELECT * FROM Articles WHERE " + boolean_query)
        return cursor.fetchall




