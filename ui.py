import re
import resources_rc
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QInputDialog, QLineEdit
import EmailSenderConfig as emailFields
from Article import Article
from Website import WebsiteConfigGenerator
from sqlite3 import IntegrityError
from WebsiteDao import get_all_websites
import DedicatedCrawler
import SharedCrawler
import TermController


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(793, 478)
        self.examplesArticlesDict = {}
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.centralwidget.setAutoFillBackground(False)
        self.centralwidget.setStyleSheet("#leftMenu {\n"
"     background-color:#67C0CE;\n"
"    border-radius: 10px;\n"
"}\n"
"\n"
"#mainBody {\n"
"    background-color:#DBE6E7;\n"
"    border-radius: 10px\n"
"}")
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leftMenu = QtWidgets.QWidget(self.centralwidget)
        self.leftMenu.setEnabled(True)
        self.leftMenu.setMaximumSize(QtCore.QSize(200, 16777215))
        self.leftMenu.setObjectName("leftMenu")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.leftMenu)
        self.verticalLayout.setObjectName("verticalLayout")
        self.crawlExframe = QtWidgets.QFrame(self.leftMenu)
        self.crawlExframe.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setKerning(True)
        self.crawlExframe.setFont(font)
        self.crawlExframe.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.crawlExframe.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.crawlExframe.setLineWidth(0)
        self.crawlExframe.setObjectName("crawlExframe")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.crawlExframe)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.crawlExlabel = QtWidgets.QLabel(self.crawlExframe)
        font = QtGui.QFont()
        font.setFamily("Century Schoolbook L")
        font.setPointSize(16)
        font.setItalic(True)
        self.crawlExlabel.setFont(font)
        self.crawlExlabel.setObjectName("crawlExlabel")
        self.verticalLayout_2.addWidget(self.crawlExlabel, 0, QtCore.Qt.AlignHCenter)
        self.verticalLayout.addWidget(self.crawlExframe)
        self.buttonsFrame = QtWidgets.QFrame(self.leftMenu)
        self.buttonsFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.buttonsFrame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.buttonsFrame.setLineWidth(0)
        self.buttonsFrame.setObjectName("buttonsFrame")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.buttonsFrame)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.inicioWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.inicioWidget.setEnabled(True)
        self.inicioWidget.setObjectName("inicioWidget")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.inicioWidget)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.inicioButton = QtWidgets.QPushButton(self.inicioWidget)
        self.inicioButton.setMinimumSize(QtCore.QSize(134, 0))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.inicioButton.setFont(font)
        self.inicioButton.setObjectName("inicioButton")
        self.horizontalLayout_5.addWidget(self.inicioButton)
        self.inicioIcon = QtWidgets.QLabel(self.inicioWidget)
        self.inicioIcon.setText("")
        self.inicioIcon.setPixmap(QtGui.QPixmap(":/whiteIcons/assets/whiteIcons/home.svg"))
        self.inicioIcon.setObjectName("inicioIcon")
        self.horizontalLayout_5.addWidget(self.inicioIcon)
        self.verticalLayout_3.addWidget(self.inicioWidget, 0, QtCore.Qt.AlignRight)
        self.fornecerWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.fornecerWidget.setObjectName("fornecerWidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.fornecerWidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.fornecerButton = QtWidgets.QPushButton(self.fornecerWidget)
        self.fornecerButton.setMinimumSize(QtCore.QSize(134, 0))
        self.fornecerButton.setObjectName("fornecerButton")
        self.horizontalLayout_2.addWidget(self.fornecerButton)
        self.fornecerIcon = QtWidgets.QLabel(self.fornecerWidget)
        self.fornecerIcon.setText("")
        self.fornecerIcon.setPixmap(QtGui.QPixmap(":/whiteIcons/assets/whiteIcons/edit.svg"))
        self.fornecerIcon.setObjectName("fornecerIcon")
        self.horizontalLayout_2.addWidget(self.fornecerIcon)
        self.verticalLayout_3.addWidget(self.fornecerWidget, 0, QtCore.Qt.AlignRight)
        self.carregarWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.carregarWidget.setObjectName("carregarWidget")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.carregarWidget)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.carregarButton = QtWidgets.QPushButton(self.carregarWidget)
        self.carregarButton.setMinimumSize(QtCore.QSize(134, 0))
        self.carregarButton.setObjectName("carregarButton")
        self.horizontalLayout_6.addWidget(self.carregarButton)
        self.carregarIcon = QtWidgets.QLabel(self.carregarWidget)
        self.carregarIcon.setText("")
        self.carregarIcon.setPixmap(QtGui.QPixmap(":/whiteIcons/assets/whiteIcons/loader.svg"))
        self.carregarIcon.setObjectName("carregarIcon")
        self.horizontalLayout_6.addWidget(self.carregarIcon)
        self.verticalLayout_3.addWidget(self.carregarWidget, 0, QtCore.Qt.AlignRight)
        self.inicializarWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.inicializarWidget.setObjectName("inicializarWidget")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.inicializarWidget)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.execute_crawler_button = QtWidgets.QPushButton(self.inicializarWidget)
        self.execute_crawler_button.setObjectName("inicializarButton")
        self.horizontalLayout_7.addWidget(self.execute_crawler_button)
        self.inicializarIcon = QtWidgets.QLabel(self.inicializarWidget)
        self.inicializarIcon.setText("")
        self.inicializarIcon.setPixmap(QtGui.QPixmap(":/whiteIcons/assets/whiteIcons/play.svg"))
        self.inicializarIcon.setObjectName("inicializarIcon")
        self.horizontalLayout_7.addWidget(self.inicializarIcon)
        self.verticalLayout_3.addWidget(self.inicializarWidget)
        self.configuracoesWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.configuracoesWidget.setObjectName("configuracoesWidget")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.configuracoesWidget)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.settings_button = QtWidgets.QPushButton(self.configuracoesWidget)
        self.settings_button.setMinimumSize(QtCore.QSize(134, 0))
        self.settings_button.setObjectName("pushButton_5")
        self.horizontalLayout_8.addWidget(self.settings_button)
        self.label_3 = QtWidgets.QLabel(self.configuracoesWidget)
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap(":/whiteIcons/assets/whiteIcons/settings.svg"))
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_8.addWidget(self.label_3)
        self.verticalLayout_3.addWidget(self.configuracoesWidget, 0, QtCore.Qt.AlignRight)
        self.versaoWidget = QtWidgets.QWidget(self.buttonsFrame)
        self.versaoWidget.setObjectName("versaoWidget")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.versaoWidget)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.versaoLabel = QtWidgets.QLabel(self.versaoWidget)
        font = QtGui.QFont()
        font.setFamily("Nimbus Mono L")
        font.setPointSize(12)
        self.versaoLabel.setFont(font)
        self.versaoLabel.setObjectName("versaoLabel")
        self.verticalLayout_4.addWidget(self.versaoLabel, 0, QtCore.Qt.AlignHCenter)
        self.verticalLayout_3.addWidget(self.versaoWidget)
        self.verticalLayout.addWidget(self.buttonsFrame)
        self.horizontalLayout.addWidget(self.leftMenu)
        self.mainBody = QtWidgets.QWidget(self.centralwidget)
        self.mainBody.setFocusPolicy(QtCore.Qt.NoFocus)
        self.mainBody.setAcceptDrops(False)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.mainBody)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.create_settings_screen()
        self.createCarregarBody()
        self.create_article_examples_screen()
        self.create_configure_crawler_screen()
        self.create_entry_point_screen()
        self.horizontalLayout.addWidget(self.mainBody)
        MainWindow.setCentralWidget(self.centralwidget)
        self.setupButtons()
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def create_popup(self, window_title: str, message: str):
        msg = QMessageBox()
        msg.setWindowTitle(window_title)
        msg.setText(message)
        msg.exec_()

    # Tela de configuração
    def create_settings_screen(self):
        self.mainBody.setObjectName("mainBody")
        #self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.mainBody)
        #self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_4 = QtWidgets.QLabel(self.mainBody)
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_4.setObjectName("label_4")
        self.verticalLayout_5.addWidget(self.label_4, 0, QtCore.Qt.AlignHCenter)
        self.configsWidget = QtWidgets.QWidget(self.mainBody)
        self.configsWidget.setMaximumSize(QtCore.QSize(16777215, 220))
        self.configsWidget.setObjectName("configsWidget")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.configsWidget)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.widget_5 = QtWidgets.QWidget(self.configsWidget)
        self.widget_5.setObjectName("widget_5")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget_5)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(self.widget_5)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.widget_5)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout_3.addWidget(self.lineEdit)
        self.verticalLayout_6.addWidget(self.widget_5)
        self.widget_4 = QtWidgets.QWidget(self.configsWidget)
        self.widget_4.setObjectName("widget_4")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.widget_4)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.labelPassword = QtWidgets.QLabel(self.widget_4)
        self.labelPassword.setObjectName("labelPassword")
        self.horizontalLayout_4.addWidget(self.labelPassword)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.widget_4)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_4.addWidget(self.lineEdit_2)
        self.verticalLayout_6.addWidget(self.widget_4)
        self.checkBox = QtWidgets.QCheckBox(self.configsWidget)
        self.checkBox.setObjectName("checkBox")
        self.verticalLayout_6.addWidget(self.checkBox)
        self.pushButton = QtWidgets.QPushButton(self.configsWidget)
        self.pushButton.setMaximumSize(QtCore.QSize(300, 16777215))
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_6.addWidget(self.pushButton, 0, QtCore.Qt.AlignHCenter)
        self.verticalLayout_5.addWidget(self.configsWidget)
        self.close_settings_screen()

    def restore_settings_default_value(self):
        if self.checkBox.isChecked():
            self.lineEdit.setEnabled(True)
            self.lineEdit_2.setEnabled(True)
        else:
            self.lineEdit.setEnabled(False)
            self.lineEdit_2.setEnabled(False)

    def load_settings_value(self):
        self.lineEdit.setText(emailFields.email)
        self.lineEdit_2.setText(emailFields.password)
        self.checkBox.setChecked(False)
        self.lineEdit.setEnabled(False)
        self.lineEdit_2.setEnabled(False)

    def open_settings_screen(self):
        self.load_settings_value()
        self.configsWidget.show()
        self.label_4.show()

    def close_settings_screen(self):
        self.label_4.close()
        self.configsWidget.close()

    def update_email_fields(self):
        emailFields.email = self.lineEdit.text().strip()
        emailFields.password = self.lineEdit_2.text().strip()
        with open('EmailSenderConfig.py', 'r+') as file:
            data = file.readlines()
            data[2] = 'email=\"' + emailFields.email + '\"\n'
            data[3] = 'password=\"' + emailFields.password + '\"\n'
            print(data)
            file.seek(0)
            file.writelines(data)
            file.close()
        self.load_settings_value()

    # Articles examples screen
    def create_article_examples_screen(self):
        self.labelInfoExamples = QtWidgets.QLabel(self.mainBody)
        self.labelInfoExamples.setMaximumSize(QtCore.QSize(16777215, 25))
        self.labelInfoExamples.setObjectName("label")
        self.verticalLayout_5.addWidget(self.labelInfoExamples, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.examplesBody = QtWidgets.QWidget(self.mainBody)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.examplesBody.sizePolicy().hasHeightForWidth())
        self.examplesBody.setSizePolicy(sizePolicy)
        self.examplesBody.setObjectName("examplesBody")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.examplesBody)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.exampleFieldsWidget = QtWidgets.QWidget(self.examplesBody)
        self.exampleFieldsWidget.setObjectName("exampleFieldsWidget")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.exampleFieldsWidget)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.urlWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.urlWidget.setObjectName("urlWidget")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.urlWidget)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.urlLabel = QtWidgets.QLabel(self.urlWidget)
        self.urlLabel.setMinimumSize(QtCore.QSize(59, 0))
        self.urlLabel.setObjectName("urlLabel")
        self.horizontalLayout_4.addWidget(self.urlLabel, 0, QtCore.Qt.AlignRight)
        self.urlLineField = QtWidgets.QLineEdit(self.urlWidget)
        self.urlLineField.setObjectName("urlLineField")
        self.horizontalLayout_4.addWidget(self.urlLineField)
        self.verticalLayout_6.addWidget(self.urlWidget)
        self.titleWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.titleWidget.setObjectName("titleWidget")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout(self.titleWidget)
        self.horizontalLayout_9.setContentsMargins(0, 9, 0, 0)
        self.horizontalLayout_9.setSpacing(0)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.titleLabel = QtWidgets.QLabel(self.titleWidget)
        self.titleLabel.setEnabled(True)
        self.titleLabel.setMinimumSize(QtCore.QSize(59, 0))
        self.titleLabel.setObjectName("titleLabel")
        self.horizontalLayout_9.addWidget(self.titleLabel)
        self.titleLineField = QtWidgets.QLineEdit(self.titleWidget)
        self.titleLineField.setObjectName("titleLineField")
        self.horizontalLayout_9.addWidget(self.titleLineField)
        self.verticalLayout_6.addWidget(self.titleWidget)
        self.contentWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.contentWidget.setObjectName("contentWidget")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout(self.contentWidget)
        self.horizontalLayout_12.setContentsMargins(0, -1, 0, 0)
        self.horizontalLayout_12.setSpacing(0)
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.contentLabel = QtWidgets.QLabel(self.contentWidget)
        self.contentLabel.setObjectName("contentLabel")
        self.horizontalLayout_12.addWidget(self.contentLabel)
        self.contentPlainText = QtWidgets.QPlainTextEdit(self.contentWidget)
        self.contentPlainText.setObjectName("contentPlainText")
        self.horizontalLayout_12.addWidget(self.contentPlainText)
        self.verticalLayout_6.addWidget(self.contentWidget)
        self.subtitleWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.subtitleWidget.setObjectName("subtitleWidget")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.subtitleWidget)
        self.horizontalLayout_10.setContentsMargins(0, -1, 0, 0)
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.subtitleLabel = QtWidgets.QLabel(self.subtitleWidget)
        self.subtitleLabel.setMinimumSize(QtCore.QSize(59, 0))
        self.subtitleLabel.setObjectName("subtitleLabel")
        self.horizontalLayout_10.addWidget(self.subtitleLabel)
        self.subtitleLineField = QtWidgets.QLineEdit(self.subtitleWidget)
        self.subtitleLineField.setObjectName("subtitleLineField")
        self.horizontalLayout_10.addWidget(self.subtitleLineField)
        self.verticalLayout_6.addWidget(self.subtitleWidget)
        self.authorWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.authorWidget.setObjectName("authorWidget")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.authorWidget)
        self.horizontalLayout_11.setContentsMargins(0, -1, 0, 0)
        self.horizontalLayout_11.setSpacing(0)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.authorLabel = QtWidgets.QLabel(self.authorWidget)
        self.authorLabel.setMinimumSize(QtCore.QSize(59, 0))
        self.authorLabel.setObjectName("authorLabel")
        self.horizontalLayout_11.addWidget(self.authorLabel, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.authorLineField = QtWidgets.QLineEdit(self.authorWidget)
        self.authorLineField.setObjectName("authorLineField")
        self.horizontalLayout_11.addWidget(self.authorLineField)
        self.verticalLayout_6.addWidget(self.authorWidget)
        self.dateWidget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.dateWidget.setObjectName("dateWidget")
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout(self.dateWidget)
        self.horizontalLayout_13.setContentsMargins(0, 9, 0, 9)
        self.horizontalLayout_13.setSpacing(0)
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.dateLineLabel = QtWidgets.QLabel(self.dateWidget)
        self.dateLineLabel.setMinimumSize(QtCore.QSize(59, 0))
        self.dateLineLabel.setObjectName("dateLineLabel")
        self.horizontalLayout_13.addWidget(self.dateLineLabel)
        self.dateLineField = QtWidgets.QLineEdit(self.dateWidget)
        self.dateLineField.setObjectName("dateLineField")
        self.dateLineField.setEnabled(False)
        self.horizontalLayout_13.addWidget(self.dateLineField)
        self.dateButton = QtWidgets.QToolButton(self.dateWidget)
        self.dateButton.setObjectName("dateButton")
        self.horizontalLayout_13.addWidget(self.dateButton)
        self.verticalLayout_6.addWidget(self.dateWidget)
        self.generate_web_site_config_button = QtWidgets.QPushButton(self.exampleFieldsWidget)
        self.generate_web_site_config_button.setObjectName("generate_web_site_config_button")
        self.verticalLayout_6.addWidget(self.generate_web_site_config_button)
        self.horizontalLayout_3.addWidget(self.exampleFieldsWidget)
        self.articlesExamplesAddedWidget = QtWidgets.QWidget(self.examplesBody)
        self.articlesExamplesAddedWidget.setMinimumSize(QtCore.QSize(100, 0))
        self.articlesExamplesAddedWidget.setMaximumSize(QtCore.QSize(125, 16777215))
        self.articlesExamplesAddedWidget.setObjectName("articlesExamplesAddedWidget")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.articlesExamplesAddedWidget)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setSpacing(9)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_2 = QtWidgets.QLabel(self.articlesExamplesAddedWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_7.addWidget(self.label_2, 0, QtCore.Qt.AlignHCenter)
        self.articlesListWidget = QtWidgets.QListWidget(self.articlesExamplesAddedWidget)
        self.articlesListWidget.setObjectName("articlesListWidget")
        self.articlesListWidget.itemDoubleClicked.connect(self.viewExample)
        self.verticalLayout_7.addWidget(self.articlesListWidget)
        self.articlesListButtonsWidget = QtWidgets.QWidget(self.articlesExamplesAddedWidget)
        self.articlesListButtonsWidget.setObjectName("articlesListButtonsWidget")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout(self.articlesListButtonsWidget)
        self.horizontalLayout_14.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_14.setSpacing(0)
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.removeArticleExampleButton = QtWidgets.QPushButton(self.articlesListButtonsWidget)
        self.removeArticleExampleButton.setObjectName("removeArticleExampleButton")
        self.removeArticleExampleButton.setIcon(QtGui.QIcon(":/whiteIcons/assets/redIcons/x.svg"))
        self.horizontalLayout_14.addWidget(self.removeArticleExampleButton)
        self.finalizeExampleButton = QtWidgets.QPushButton(self.articlesListButtonsWidget)
        self.finalizeExampleButton.setObjectName("finalizeExampleButton")
        self.finalizeExampleButton.setIcon(QtGui.QIcon(":/whiteIcons/assets/greenIcons/check.svg"))
        self.horizontalLayout_14.addWidget(self.finalizeExampleButton)
        self.verticalLayout_7.addWidget(self.articlesListButtonsWidget)
        self.horizontalLayout_3.addWidget(self.articlesExamplesAddedWidget)
        self.verticalLayout_5.addWidget(self.examplesBody)
        self.close_articles_examples_screen()

    def close_articles_examples_screen(self):
        self.labelInfoExamples.close()
        self.examplesBody.close()
        
    def open_articles_examples_screen(self):
        self.labelInfoExamples.show()
        self.examplesBody.show()

    def show_calendar(self):
        self.calendarWidget = QtWidgets.QMainWindow(self.examplesBody)
        self.calendarWidget.setObjectName("calendarWidget")
        self.calendarWidget.resize(378, 194)
        self.calendarWidget.setFixedSize(QtCore.QSize(378, 194))
        self.calendarWidget.setWindowFlag(QtCore.Qt.WindowMinimizeButtonHint, False)
        self.calendar = QtWidgets.QCalendarWidget(self.calendarWidget)
        self.calendar.setMinimumSize(QtCore.QSize(378, 194))
        self.calendar.setAcceptDrops(False)
        self.calendar.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.calendar.setGridVisible(False)
        self.calendar.setSelectionMode(QtWidgets.QCalendarWidget.SingleSelection)
        self.calendar.setHorizontalHeaderFormat(QtWidgets.QCalendarWidget.ShortDayNames)
        self.calendar.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)
        self.calendar.setNavigationBarVisible(True)
        self.calendar.setDateEditEnabled(True)
        self.calendar.setObjectName("calendar")
        self.calendar.setLocale(QtCore.QLocale(QtCore.QLocale.Portuguese))
        self.calendar.activated.connect(self.setCalendarDate)
        QtCore.QMetaObject.connectSlotsByName(self.calendarWidget)
        _translate = QtCore.QCoreApplication.translate
        self.calendarWidget.setWindowTitle(_translate("calendarWidget", "Calendário"))
        self.calendarWidget.show()
    
    def setCalendarDate(self):
        self.dateLineField.setText(self.calendar.selectedDate().toString())
        self.calendarWidget.close()
        self.calendarWidget.destroy()

    def add_example_article(self):
        article = self.__getMandatoryFields()
        if article is not None:
            if article.get_url() in self.examplesArticlesDict:
                self.create_popup("Alerta", "Esta URL já foi adicionada aos exemplos")
                raise Exception("URL já adicionada aos exemplos")
            self.articlesListWidget.addItem(article.get_url())
            self.examplesArticlesDict[article.get_url()] = article
            self.__clearExampleFields()

    def removeExample(self):
        for item in self.articlesListWidget.selectedItems():
            self.articlesListWidget.takeItem(self.articlesListWidget.row(item))
            self.examplesArticlesDict.pop(item.text())
            self.__clearExampleFields()

    def viewExample(self):
        msg = QMessageBox()
        msg.setWindowTitle("Artigo")
        article = self.examplesArticlesDict.get(self.articlesListWidget.currentItem().text())
        msg.setText(str(article))
        msg.exec_()
   
    def __getMandatoryFields(self):
        url = self.urlLineField.text().strip()
        title = self.titleLineField.text().strip()
        content = self.contentPlainText.toPlainText().strip()
        print(self.dateLineField.text().strip())
        if url == "" or title == "" or content == "":
            self.create_popup("Alerta", "Os campos: URL, título e conteúdo são obrigatórios")
            return None
        date = self.dateLineField.text().strip()
        subtitle = self.subtitleLineField.text().strip()
        author = self.authorLineField.text().strip()
        return Article(url, title, content, date, subtitle, author)

    def __clearExampleFields(self):
        self.urlLineField.setText("")
        self.titleLineField.setText("")
        self.contentPlainText.setPlainText("")
        self.dateLineField.setText("")
        self.subtitleLineField.setText("")
        self.authorLineField.setText("")

    def generate_website_config_action(self):
        examples_articles = []
        for example_article_url in self.examplesArticlesDict:
            examples_articles.append(self.examplesArticlesDict[example_article_url])
        website_config_generator = WebsiteConfigGenerator(examples_articles)
        try:
            web_site_start_url = website_config_generator.generate()
            window_title = 'INFO'
            window_message = 'Configuração gerada para o website ' + web_site_start_url
        except IndexError:
            window_title = 'ERRO'
            window_message = 'É necessário que ao menos um artigo exemplo seja adicionado.'
        except IntegrityError:
            window_title = 'ERRO'
            window_message = 'Não foi possível gerar a configuração, pois o website solicitado já está na base.'
        self.examplesArticlesDict.clear()
        self.articlesListWidget.clear()
        msg = QMessageBox()
        msg.setWindowTitle(window_title)
        msg.setText(window_message)
        msg.exec_()


    def create_entry_point_screen(self):
        self.entry_point_widget = QtWidgets.QWidget(self.mainBody)
        self.entry_point_widget.setObjectName("entry_point_widget")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.entry_point_widget)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.entry_point_text_browser = QtWidgets.QTextBrowser(self.entry_point_widget)
        self.entry_point_text_browser.setObjectName("entry_point_text_browser")
        self.verticalLayout_6.addWidget(self.entry_point_text_browser)
        self.verticalLayout_5.addWidget(self.entry_point_widget)
        self.horizontalLayout.addWidget(self.mainBody)
        self.__highlight_button(self.inicioButton)


    # Tela carregar fonte
    def createCarregarBody(self):
        self.carregarBody = QtWidgets.QWidget(self.mainBody)
        self.carregarBody.setObjectName("carregarBody")
        self.carregarBodyVerticalLayout = QtWidgets.QVBoxLayout(self.carregarBody)
        self.carregarBodyVerticalLayout.setObjectName("carregarBodyVerticalLayout")
        self.carregarBodyWidget = QtWidgets.QWidget(self.carregarBody)
        self.carregarBodyWidget.setObjectName("carregarBodyWidget")
        self.carregarBodyHorizontalLayout = QtWidgets.QHBoxLayout(self.carregarBodyWidget)
        self.carregarBodyHorizontalLayout.setObjectName("carregarBodyHorizontalLayout")
        self.carregarBodyLabel = QtWidgets.QLabel(self.carregarBodyWidget)
        self.carregarBodyLabel.setMaximumSize(QtCore.QSize(70, 16777215))
        self.carregarBodyLabel.setObjectName("carregarBodyLabel")
        self.carregarBodyHorizontalLayout.addWidget(self.carregarBodyLabel, 0, QtCore.Qt.AlignLeft)
        self.select_website_combo_box = QtWidgets.QComboBox(self.carregarBodyWidget)
        self.select_website_combo_box.setMinimumSize(QtCore.QSize(500, 0))
        self.select_website_combo_box.setObjectName("selecionarFonteComboBox")
        self.carregarBodyHorizontalLayout.addWidget(self.select_website_combo_box)
        self.carregarBodyHorizontalLayout.setAlignment(QtCore.Qt.AlignCenter)
        self.carregarBodyVerticalLayout.addWidget(self.carregarBodyWidget)
        self.carregarBodyScrollArea = QtWidgets.QPlainTextEdit(self.carregarBody)
        self.carregarBodyScrollArea.setObjectName("carregarBodyScrollArea")
        self.carregarBodyScrollAreaWidgetContents = QtWidgets.QWidget()
        self.carregarBodyScrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 555, 340))
        self.carregarBodyScrollAreaWidgetContents.setObjectName("carregarBodyScrollAreaWidgetContents")
        self.carregarBodyVerticalLayout.addWidget(self.carregarBodyScrollArea)
        self.carregarBodyWidget_2 = QtWidgets.QWidget(self.carregarBody)
        self.carregarBodyWidget_2.setObjectName("carregarBodyWidget_2")
        self.carregarBodyHorizontalLayout2 = QtWidgets.QHBoxLayout(self.carregarBodyWidget_2)
        self.carregarBodyHorizontalLayout2.setObjectName("carregarBodyHorizontalLayout2")
        self.paraColetaButton = QtWidgets.QPushButton(self.carregarBodyWidget_2)
        self.paraColetaButton.setObjectName("paraColetaButton")
        self.paraColetaButton.setIcon(QtGui.QIcon(":/whiteIcons/assets/redIcons/stop-circle.svg"))
        self.carregarBodyHorizontalLayout2.addWidget(self.paraColetaButton)
        self.iniciarColetaButton = QtWidgets.QPushButton(self.carregarBodyWidget_2)
        self.iniciarColetaButton.setObjectName("iniciarColetaButton")
        self.iniciarColetaButton.setIcon(QtGui.QIcon(":/whiteIcons/assets/greenIcons/play.svg"))
        self.carregarBodyHorizontalLayout2.addWidget(self.iniciarColetaButton)
        self.carregarBodyVerticalLayout.addWidget(self.carregarBodyWidget_2)
        self.verticalLayout_5.addWidget(self.carregarBody)
        self.carregarBodyScrollArea.setReadOnly(True)
        self.carregarBody.close()

    def setupButtons(self):
        self.inicioButton.clicked.connect(lambda: self.open_widget_in_main_body(self.entry_point_widget, self.inicioButton))
        self.fornecerButton.clicked.connect(lambda: self.open_widget_in_main_body(self.examplesBody, self.fornecerButton))
        self.carregarButton.clicked.connect(lambda: self.open_widget_in_main_body(self.carregarBody, self.carregarButton))
        self.settings_button.clicked.connect(lambda: self.open_widget_in_main_body(self.configsWidget, self.settings_button))
        self.execute_crawler_button.clicked.connect(lambda: self.open_widget_in_main_body(self.configure_crawler_widget, self.execute_crawler_button))
        self.checkBox.clicked.connect(self.restore_settings_default_value)
        self.pushButton.clicked.connect(self.update_email_fields)
        self.dateButton.clicked.connect(self.show_calendar)
        self.finalizeExampleButton.clicked.connect(self.add_example_article)
        self.removeArticleExampleButton.clicked.connect(self.removeExample)
        self.generate_web_site_config_button.clicked.connect(self.generate_website_config_action)
        self.iniciarColetaButton.clicked.connect(self.start_collect_action)
        self.paraColetaButton.clicked.connect(self.stop_collect_action)
        self.add_website_to_execute_button.clicked.connect(self.add_website_to_execute_action)
        self.remove_website_button.clicked.connect(self.remove_website_to_execute_action)
        self.configure_crawler_for_websites_button.clicked.connect(self.configure_crawler_for_websites_action)

    def start_collect_action(self):
        self.carregarBodyScrollArea.clear()
        start_url = tuple(self.select_website_combo_box.currentText().split(' - '))[1]
        self.dedicated_crawler = DedicatedCrawler.DedicatedCrawler(start_url)
        print('Starting collect...')
        self.dedicated_crawler.start()
        self.dedicated_crawler.finished.connect(self.collect_finished)
        self.dedicated_crawler.update_log.connect(self.evt_update_log)

    def collect_finished(self):
        QMessageBox.information(self.carregarBodyScrollArea, "INFO", "Simulação finalizada!")

    def evt_update_log(self, val):
        if self.carregarBodyScrollArea.toPlainText() == "":
            self.carregarBodyScrollArea.setPlainText(val)
            return
        self.carregarBodyScrollArea.setPlainText(self.carregarBodyScrollArea.toPlainText() + '\n\n' + val)
        scroll_position = self.carregarBodyScrollArea.verticalScrollBar().maximum()
        self.carregarBodyScrollArea.verticalScrollBar().setValue(scroll_position)


    def stop_collect_action(self):
        self.dedicated_crawler.stop_crawl()

    def __highlight_button(self, button: QtWidgets.QPushButton):
        elegible_buttons = [self.inicioButton, self.fornecerButton, self.carregarButton, self.execute_crawler_button,
                           self.settings_button]
        button.setEnabled(False)
        for b in elegible_buttons:
            if b is not button:
                b.setEnabled(True)

    def get_widgets_in_main_body(self):
        return [self.configsWidget, self.carregarBody, self.examplesBody, self.configure_crawler_widget,
                self.entry_point_widget]

    def open_widget_in_main_body(self, widget: QtWidgets.QWidget, button: QtWidgets.QPushButton):
        self.__highlight_button(button)
        for screen in self.get_widgets_in_main_body():
            screen.close()
            if screen == self.configsWidget:
                self.label_4.close()
            if screen == self.examplesBody:
                self.close_articles_examples_screen()
            if screen == self.carregarBody:
                self.select_website_combo_box.clear()
            if screen == self.configure_crawler_widget:
                self.close_configure_crawler_screen()
                self.website_to_execute_combo_box.clear()
        widget.show()
        if widget == self.configsWidget:
            self.open_settings_screen()
        if widget == self.examplesBody:
            self.open_articles_examples_screen()
        if widget == self.configure_crawler_widget:
            self.open_configure_crawler_screen()
            websites = get_all_websites()
            for website in websites:
                self.website_to_execute_combo_box.addItem(str(website[0]) + ' - ' + website[1])
        if widget == self.carregarBody:
            websites = get_all_websites()
            for website in websites:
                self.select_website_combo_box.addItem(str(website[0]) + ' - ' + website[1])

    #configure crawler screen
    def create_configure_crawler_screen(self):
        self.select_websites_title_label = QtWidgets.QLabel(self.mainBody)
        self.select_websites_title_label.setMaximumSize(QtCore.QSize(16777215, 25))
        self.select_websites_title_label.setObjectName("select_websites_title_label")
        self.verticalLayout_5.addWidget(self.select_websites_title_label, 0,
                                        QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.configure_crawler_widget = QtWidgets.QWidget(self.mainBody)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.configure_crawler_widget.sizePolicy().hasHeightForWidth())
        self.configure_crawler_widget.setSizePolicy(sizePolicy)
        self.configure_crawler_widget.setObjectName("configure_crawler_widget")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.configure_crawler_widget)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.exampleFieldsWidget = QtWidgets.QWidget(self.configure_crawler_widget)
        self.exampleFieldsWidget.setObjectName("exampleFieldsWidget")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.exampleFieldsWidget)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.add_and_select_website_to_execute_widget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.add_and_select_website_to_execute_widget.setObjectName("add_and_select_website_to_execute_widget")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.add_and_select_website_to_execute_widget)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.website_to_execute_combo_box = QtWidgets.QComboBox(self.add_and_select_website_to_execute_widget)
        self.website_to_execute_combo_box.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.website_to_execute_combo_box.sizePolicy().hasHeightForWidth())
        self.website_to_execute_combo_box.setSizePolicy(sizePolicy)
        self.website_to_execute_combo_box.setMinimumSize(QtCore.QSize(270, 0))
        self.website_to_execute_combo_box.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.website_to_execute_combo_box.setSizeIncrement(QtCore.QSize(0, 0))
        self.website_to_execute_combo_box.setBaseSize(QtCore.QSize(0, 0))
        self.website_to_execute_combo_box.setObjectName("website_to_execute_combo_box")
        self.website_to_execute_combo_box.addItem("")
        self.horizontalLayout_4.addWidget(self.website_to_execute_combo_box, 0, QtCore.Qt.AlignVCenter)
        self.add_website_to_execute_button = QtWidgets.QPushButton(self.add_and_select_website_to_execute_widget)
        self.add_website_to_execute_button.setMaximumSize(QtCore.QSize(120, 16777215))
        self.add_website_to_execute_button.setObjectName("add_website_to_execute_button")
        self.horizontalLayout_4.addWidget(self.add_website_to_execute_button)
        self.verticalLayout_6.addWidget(self.add_and_select_website_to_execute_widget)
        self.all_this_words_widget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.all_this_words_widget.setObjectName("all_this_words_widget")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.all_this_words_widget)
        self.verticalLayout_8.setContentsMargins(0, 9, 0, 9)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.all_this_words_label = QtWidgets.QLabel(self.all_this_words_widget)
        self.all_this_words_label.setObjectName("all_this_words_label")
        self.verticalLayout_8.addWidget(self.all_this_words_label, 0, QtCore.Qt.AlignHCenter)
        self.all_this_words_line_edit = QtWidgets.QLineEdit(self.all_this_words_widget)
        self.all_this_words_line_edit.setObjectName("all_this_words_line_edit")
        self.verticalLayout_8.addWidget(self.all_this_words_line_edit)
        self.verticalLayout_6.addWidget(self.all_this_words_widget)
        self.exactly_this_phrase_widget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.exactly_this_phrase_widget.setObjectName("exactly_this_phrase_widget")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout(self.exactly_this_phrase_widget)
        self.verticalLayout_10.setContentsMargins(0, -1, 0, 9)
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.exactly_this_phrase_label = QtWidgets.QLabel(self.exactly_this_phrase_widget)
        self.exactly_this_phrase_label.setObjectName("exactly_this_phrase_label")
        self.verticalLayout_10.addWidget(self.exactly_this_phrase_label, 0, QtCore.Qt.AlignHCenter)
        self.exactly_this_phrase_line_edit = QtWidgets.QLineEdit(self.exactly_this_phrase_widget)
        self.exactly_this_phrase_line_edit.setObjectName("exactly_this_phrase_line_edit")
        self.verticalLayout_10.addWidget(self.exactly_this_phrase_line_edit)
        self.verticalLayout_6.addWidget(self.exactly_this_phrase_widget)
        self.any_of_this_words_widget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.any_of_this_words_widget.setObjectName("any_of_this_words_widget")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.any_of_this_words_widget)
        self.verticalLayout_9.setContentsMargins(0, -1, 0, 9)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.any_of_this_words_label = QtWidgets.QLabel(self.any_of_this_words_widget)
        self.any_of_this_words_label.setObjectName("any_of_this_words_label")
        self.verticalLayout_9.addWidget(self.any_of_this_words_label, 0, QtCore.Qt.AlignHCenter)
        self.any_of_this_words_line_edit = QtWidgets.QLineEdit(self.any_of_this_words_widget)
        self.any_of_this_words_line_edit.setObjectName("any_of_this_words_line_edit")
        self.verticalLayout_9.addWidget(self.any_of_this_words_line_edit)
        self.verticalLayout_6.addWidget(self.any_of_this_words_widget)
        self.emails_to_be_sent_widget = QtWidgets.QWidget(self.exampleFieldsWidget)
        self.emails_to_be_sent_widget.setObjectName("emails_to_be_sent_widget")
        self.verticalLayout_11 = QtWidgets.QVBoxLayout(self.emails_to_be_sent_widget)
        self.verticalLayout_11.setObjectName("verticalLayout_11")
        self.emails_to_be_sent_label = QtWidgets.QLabel(self.emails_to_be_sent_widget)
        self.emails_to_be_sent_label.setObjectName("emails_to_be_sent_label")
        self.verticalLayout_11.addWidget(self.emails_to_be_sent_label, 0, QtCore.Qt.AlignHCenter)
        self.emails_to_be_sent_line_edit = QtWidgets.QLineEdit(self.emails_to_be_sent_widget)
        self.emails_to_be_sent_line_edit.setObjectName("emails_to_be_sent_line_edit")
        self.verticalLayout_11.addWidget(self.emails_to_be_sent_line_edit)
        self.verticalLayout_6.addWidget(self.emails_to_be_sent_widget)
        self.configure_crawler_for_websites_button = QtWidgets.QPushButton(self.exampleFieldsWidget)
        self.configure_crawler_for_websites_button.setObjectName("configure_crawler_for_websites_button")
        self.verticalLayout_6.addWidget(self.configure_crawler_for_websites_button)
        self.horizontalLayout_3.addWidget(self.exampleFieldsWidget)
        self.websites_to_execute_widget = QtWidgets.QWidget(self.configure_crawler_widget)
        self.websites_to_execute_widget.setMinimumSize(QtCore.QSize(100, 0))
        self.websites_to_execute_widget.setMaximumSize(QtCore.QSize(125, 16777215))
        self.websites_to_execute_widget.setObjectName("websites_to_execute_widget")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.websites_to_execute_widget)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setSpacing(9)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.websites_to_execute_label = QtWidgets.QLabel(self.websites_to_execute_widget)
        self.websites_to_execute_label.setMinimumSize(QtCore.QSize(119, 0))
        self.websites_to_execute_label.setObjectName("websites_to_execute_label")
        self.verticalLayout_7.addWidget(self.websites_to_execute_label, 0, QtCore.Qt.AlignHCenter)
        self.website_to_execute_list_widget = QtWidgets.QListWidget(self.websites_to_execute_widget)
        self.website_to_execute_list_widget.setObjectName("website_to_execute_list_widget")
        self.verticalLayout_7.addWidget(self.website_to_execute_list_widget)
        self.articlesListButtonsWidget = QtWidgets.QWidget(self.websites_to_execute_widget)
        self.articlesListButtonsWidget.setObjectName("articlesListButtonsWidget")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout(self.articlesListButtonsWidget)
        self.horizontalLayout_14.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_14.setSpacing(0)
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.remove_website_button = QtWidgets.QPushButton(self.articlesListButtonsWidget)
        self.remove_website_button.setObjectName("remove_website_button")
        self.horizontalLayout_14.addWidget(self.remove_website_button)
        self.verticalLayout_7.addWidget(self.articlesListButtonsWidget)
        self.horizontalLayout_3.addWidget(self.websites_to_execute_widget)
        self.verticalLayout_5.addWidget(self.configure_crawler_widget)
        self.close_configure_crawler_screen()

    def close_configure_crawler_screen(self):
        self.select_websites_title_label.close()
        self.configure_crawler_widget.close()

    def open_configure_crawler_screen(self):
        self.select_websites_title_label.show()
        self.configure_crawler_widget.show()

    def add_website_to_execute_action(self):
        website_to_execute = self.website_to_execute_combo_box.currentText()
        for i in range(self.website_to_execute_list_widget.count()):
            if self.website_to_execute_list_widget.item(i).text() == website_to_execute:
                return
        self.website_to_execute_list_widget.addItem(website_to_execute)

    def remove_website_to_execute_action(self):
        for item in self.website_to_execute_list_widget.selectedItems():
            self.website_to_execute_list_widget.takeItem(self.website_to_execute_list_widget.row(item))

    def configure_crawler_for_websites_action(self):
        if self.website_to_execute_list_widget.count() == 0:
            QMessageBox.information(self.configure_crawler_widget, "ALERTA", "É necessário adicionar pelo menos um "
                                                                             "website para executar!")
            return
        all_this_words = set(word for word in self.all_this_words_line_edit.text().split())
        exactly_this_phrase = self.exactly_this_phrase_line_edit.text().strip()
        any_of_this_words = set(word for word in self.any_of_this_words_line_edit.text().split())
        emails_to_be_sent = set(email for email in self.emails_to_be_sent_line_edit.text().split())
        crawler_name, crawler_name_ok = QInputDialog().getText(self.configure_crawler_widget, "Identificação do crawler",
                                                               "Dê um nome para a identificação do crawler", QLineEdit.Normal)
        if not crawler_name_ok:
            return

        text, ok = QInputDialog().getText(self.configure_crawler_widget, crawler_name,
                                          "Quantos segundos cada fonte deverá executar?", QLineEdit.Normal)

        #remover
        print(all_this_words)
        print(exactly_this_phrase)
        print(any_of_this_words)
        print(emails_to_be_sent)

        term_controller = TermController.TermController(all_this_words, exactly_this_phrase, any_of_this_words)

        start_urls = []
        for i in range(self.website_to_execute_list_widget.count()):
            id_and_site = self.website_to_execute_list_widget.item(i).text()
            start_url = re.sub('\\d+ - ', '', id_and_site)
            start_urls.append(start_url)

        if text.strip() and ok:
            shared_crawler = SharedCrawler.SharedCrawler(crawler_name, start_urls, int(text.strip()), term_controller,
                                                         emails_to_be_sent)
            shared_crawler.start()
            QMessageBox.information(self.configure_crawler_widget, "INFO", "Execução do crawler " + crawler_name +
                                    " iniciada")


    ###################################################################################################################
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "CrawlEX"))
        self.crawlExlabel.setText(_translate("MainWindow", "CrawlEX"))
        self.inicioButton.setText(_translate("MainWindow", "Início"))
        self.fornecerButton.setText(_translate("MainWindow", "Fornecer exemplos"))
        self.carregarButton.setText(_translate("MainWindow", "Carregar fonte"))
        self.execute_crawler_button.setText(_translate("MainWindow", "Executar um Crawler"))
        self.settings_button.setText(_translate("MainWindow", "Configurações"))
        self.versaoLabel.setText(_translate("MainWindow", "Versão 1.0.0"))
        self.labelInfoExamples.setText(_translate("MainWindow", "Forneça artigos exemplos para configurar a coleta do website"))
        self.label_4.setText(_translate("MainWindow", "Informe o endereço de e-mail e senha de uma conta Google que será o robô de envio de relatórios"))
        self.label.setText(_translate("MainWindow", "Email"))
        self.labelPassword.setText(_translate("MainWindow", "Senha"))
        self.label_2.setText(_translate("MainWindow", "Artigos exemplos"))
        self.checkBox.setText(_translate("MainWindow", "Editar"))
        self.pushButton.setText(_translate("MainWindow", "Salvar email e senha"))
        self.carregarBodyLabel.setText(_translate("MainWindow", "Website"))
        self.urlLabel.setText(_translate("MainWindow", "URL"))
        self.titleLabel.setText(_translate("MainWindow", "Título"))
        self.contentLabel.setText(_translate("MainWindow", "Conteúdo"))
        self.subtitleLabel.setText(_translate("MainWindow", "Subtítulo"))
        self.authorLabel.setText(_translate("MainWindow", "Autor"))
        self.dateLineLabel.setText(_translate("MainWindow", "Data"))
        self.dateButton.setText(_translate("MainWindow", "..."))
        self.generate_web_site_config_button.setText(_translate("MainWindow", "Gerar configuração para coleta do website"))
        self.select_websites_title_label.setText(
            _translate("MainWindow", "Selecione os websites a executar e defina os termos de seu interesse"))
        self.add_website_to_execute_button.setText(_translate("MainWindow", "Adicionar"))
        self.all_this_words_label.setText(_translate("MainWindow", "Todas estas palavras:"))
        self.exactly_this_phrase_label.setText(_translate("MainWindow", "Exatamente esta frase:"))
        self.any_of_this_words_label.setText(_translate("MainWindow", "Quaisquer destas palavras:"))
        self.emails_to_be_sent_label.setText(_translate("MainWindow", "Emails a serem enviados relatório de coleta"))
        self.configure_crawler_for_websites_button.setText(
            _translate("MainWindow", "Executar Crawler para websites selecionados"))
        self.websites_to_execute_label.setText(_translate("MainWindow", "Websites a executar"))
        self.remove_website_button.setText(_translate("MainWindow", "Remover website"))
        self.entry_point_text_browser.setHtml(_translate("MainWindow",
                                                         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                         "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                                         "p, li { white-space: pre-wrap; }\n"
                                                         "</style></head><body style=\" font-family:\'Ubuntu\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">BEM VINDO AO CRAWLEX</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1 - FORNECER EXEMPLOS</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Utilize esta tela para informar artigos exemplos de sites que você tem interesse em coletar informações. É importante que todos os exemplos de um mesmo site, contenham os mesmos campos preenchidos. Por exemplo, se for informado a data para o primeiro exemplo, informe para os demais também. Aconselha-se fornecer dois ou três exemplos por site.</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2 - CARREGAR FONTE</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Utilize para simular a navegação e coleta de um site em que os artigos exemplos já foram fornecidos e estão salvos na base de dados.</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3 - EXECUTAR UM CRAWLER</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Utilize para selecionar todos os sites desejados (desde que tiveram artigos exemplos fornecidos previamente) a fim de executar um crawler com eles. Preencha os campos de busca de acordo com o seu interesse e forneça os emails aos quais receberão informações para a coleta.</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">4 - CONFIGURAÇÕES</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Utilize para setar o email e a senha de uma conta google, que será a responsável por fazer os envios de emails com informações das coletas do crawler.</p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
                                                         "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))

